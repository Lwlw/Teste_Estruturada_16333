﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teste_Estruturada_16333
{
    class ListaCodPostal
    {

        public static List<CodigosPostai> LoadCodPostais()
        {
            //List<DataClasses1DataContext> output = new List<DataClasses1DataContext>();

            DataClasses1DataContext dc = new DataClasses1DataContext();

            List<CodigosPostai> output = new List<CodigosPostai>();

            var carrega = from CodigosPostai in dc.CodigosPostais select CodigosPostai;

            foreach (var cod in carrega)
            {
                output.Add(cod);
            }

            return output;
        }
    }
}
