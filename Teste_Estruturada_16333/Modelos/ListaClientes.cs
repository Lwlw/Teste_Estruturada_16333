﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teste_Estruturada_16333
{
    
    class ListaClientes
    {

        public static List<Cliente> LoadClientes()
        {
            //List<DataClasses1DataContext> output = new List<DataClasses1DataContext>();

            DataClasses1DataContext dc = new DataClasses1DataContext();

            List<Cliente> output = new List<Cliente>();
             
            var carrega = from Cliente in dc.Clientes select Cliente;

            foreach (var cli in carrega)
            {
                output.Add(cli);
            }

            return output;
        }



    }
}
