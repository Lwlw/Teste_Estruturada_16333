﻿namespace Teste_Estruturada_16333
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;
    public partial class Form1 : Form
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        List<Cliente> Clientes = ListaClientes.LoadClientes();
        List<CodigosPostai> CodPostais = ListaCodPostal.LoadCodPostais();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            loadCombo();
        }

        private void loadCombo()
        {
           
            toolStripComboBox.Items.Clear();
            var listc = Clientes.OrderBy(x => x.ID).ToList();
            foreach (var n in listc)
                toolStripComboBox.Items.Add(n.ID);
            toolStripComboBox.SelectedIndex = 0;
        }

        private void toolStripButtonSearch_Click(object sender, EventArgs e)
        {
            var lista = Clientes.Where(x => x.ID == Convert.ToInt32(toolStripComboBox.Text)).ToList();
            var lista2 = CodPostais.Where(x => x.IdCliente == Convert.ToInt32(toolStripComboBox.Text)).ToList();

            textBoxNome.Text = lista[0].Nome;
            textBoxID.Text = lista[0].ID.ToString();
            textBoxMorada.Text = lista[0].Morada;
            textBoxTelefone.Text = lista[0].Telefone.ToString();
            textBoxCodigoPost.Text = lista2[0].Cod_Postal;
            textBoxLocalidade.Text = lista2[0].Localidade;
        }


        //NOVO
        private void toolStripButtonCriar_Click(object sender, EventArgs e)
        {
            bool digitsOnly = textBoxID.Text.All(char.IsDigit);
            if (!digitsOnly)
            {
                MessageBox.Show($"O campo ID recebe apenas digitos");
                return;
            }

            //Se algum dos campos se encontrar vazio, sai
            if (vazioVerificar())
                return;

            //Se existir o ID, sai
            var listaIDs = Clientes.Where(x => x.ID == Convert.ToInt32(textBoxID.Text)).ToList();
            if (listaIDs.Count != 0)
            {
                MessageBox.Show($"Já existe um cliente com o ID {textBoxID.Text}");
                return;
            }


            ////adiciona objecto à lista e grava na BD
            try
            {
                var cli = new Cliente
                {
                    Nome = textBoxNome.Text,
                    ID = Convert.ToInt32(textBoxID.Text),
                    Telefone = Convert.ToInt32(textBoxTelefone.Text),
                    Morada = textBoxMorada.Text,
                };

                Clientes.Add(cli);
                dc.Clientes.InsertOnSubmit(cli);

                var cod = new CodigosPostai
                {
                    Cod_Postal = textBoxCodigoPost.Text,
                    IdCliente = Convert.ToInt32(textBoxID.Text),
                    Localidade = textBoxLocalidade.Text
                };

                CodPostais.Add(cod);
                dc.CodigosPostais.InsertOnSubmit(cod);

                try
                {
                    dc.SubmitChanges();
                    MessageBox.Show("Cliente criado com sucesso");

                }
                catch (Exception h)
                {
                    Console.WriteLine(h.Message);
                }

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }

            loadCombo();
        }

        int campo = 0;
        private bool vazioVerificar()
        {
            string[] campos = { "ID", "Nome", "Morada", "Código Postal", "Localidade", "Telefone" };

            if (string.IsNullOrEmpty(textBoxID.Text)) { campo = 1; Message(); return true; }
            if (string.IsNullOrEmpty(textBoxNome.Text)) { campo = 2; Message(); return true; }
            if (string.IsNullOrEmpty(textBoxMorada.Text)) { campo = 3; Message(); return true; }
            if (string.IsNullOrEmpty(textBoxCodigoPost.Text)) { campo = 5; Message(); return true; }
            if (string.IsNullOrEmpty(textBoxLocalidade.Text)) { campo = 6; Message(); return true; }
            if (string.IsNullOrEmpty(textBoxTelefone.Text)) { campo = 4; Message(); return true; }

            void Message() { MessageBox.Show($"Insira um valor para {campos[campo - 1]}"); }

            return false;
        }


        //APAGAR
        private void toolStripButtonApagar_Click(object sender, EventArgs e)
        {

            DialogResult result = MessageBox.Show(
                "Tem  a certeza que deseja apagar o cliente?", "Confirmação", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.No || result == DialogResult.Cancel)
            {
                return;
            }

            CodigosPostai cod = new CodigosPostai();

            var Pesquisa1 = from CodigosPostai in dc.CodigosPostais
                            where CodigosPostai.IdCliente == Convert.ToInt32(textBoxID.Text)
                            select CodigosPostai;

            cod = Pesquisa1.Single();

            dc.CodigosPostais.DeleteOnSubmit(cod);
            CodPostais.Remove(cod);

            Cliente cli = new Cliente();

            var Pesquisa = from Cliente in dc.Clientes
                                where Cliente.ID == Convert.ToInt32(textBoxID.Text)
                                select Cliente;

            cli = Pesquisa.Single();

            dc.Clientes.DeleteOnSubmit(cli);
            Clientes.Remove(cli);

            try
            {
                dc.SubmitChanges();
                MessageBox.Show("Cliente apagado com sucesso");
            }
            catch (Exception c)
            {
                MessageBox.Show(c.Message);
            }

            loadCombo();
            clearFields();
        }

        private void clearFields()
        {

            textBoxID.Text = String.Empty; 
            textBoxNome.Text = String.Empty;
            textBoxMorada.Text = String.Empty;
            textBoxTelefone.Text = String.Empty;
            textBoxCodigoPost.Text = String.Empty;
            textBoxMorada.Text = String.Empty;
            textBoxLocalidade.Text = String.Empty;
        }



        //ALTERAR
        private void toolStripButtonAlterar_Click(object sender, EventArgs e)
        {
            bool digitsOnly = textBoxID.Text.All(char.IsDigit);
            if (!digitsOnly)
            {
                MessageBox.Show($"O campo ID recebe apenas digitos");
                return;
            }

            //Se algum dos campos se encontrar vazio, sai e envia msg
            if (vazioVerificar())
                return;

            var pesquisa = from Cliente in dc.Clientes
                           where Cliente.ID == Convert.ToInt32(textBoxID.Text)
                           select Cliente;

            var pesquisa1 = from CodigosPostai in dc.CodigosPostais
                           where CodigosPostai.IdCliente == Convert.ToInt32(textBoxID.Text)
                           select CodigosPostai; 

            var aAlterar = pesquisa.Single();
            aAlterar.Nome = textBoxNome.Text;
            aAlterar.Morada = textBoxMorada.Text;
            aAlterar.Telefone = Convert.ToInt32(textBoxTelefone.Text);

            var aAlterar1 = pesquisa1.Single();
            aAlterar1.Cod_Postal = textBoxCodigoPost.Text;
            aAlterar1.Localidade = textBoxMorada.Text;


            try
            {
                dc.SubmitChanges();
                MessageBox.Show("Cliente alterado com sucesso");
            }
            catch (Exception t)
            {
                Console.WriteLine(t.Message);
            }

        }

        private void toolStripButtonCancelar_Click(object sender, EventArgs e)
        {
            clearFields();
        }
    }
}
